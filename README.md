# Services



## Getting started

This is the service linked to backend DB and autosigner. 

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/open-source-metx-games/services.git
git branch -M main
git push -uf origin main
```
## Models
Add the relevant initialization autosigner links and credentials. This service is based on singleton architecture which revolves upon static models and credentials.

## Scoring
Add the following command to update the scoring system

```
int currentScore = Models.Score;
Models.Score = currentScore + reward;
Debug.Log(Models.Score);
```

## UI Manager
Add Gameobject for ClaimButton upon winning or death.
Add GameObjects for Utils linking Utils.cs script and claimbutton gameobject in the unity editor.

In the Existing UI Script enter:

```
claimbutton.SetActive(true);
claimbutton.GetComponent<Button>().onClick.AddListener(DisableButton);

private void DisableButton()
    {
        claimbutton.SetActive(false);
    }
```

## Authors and acknowledgment
Adil, Eiman

## License
Mit? Dunno

## Project status
On Going